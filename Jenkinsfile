pipeline {
  agent {
    kubernetes {
      yaml '''
        apiVersion: v1
        kind: Pod
        spec:
          containers:
          - name: docker
            image: docker:latest
            command:
            - cat
            tty: true
            volumeMounts:
             - mountPath: /var/run/docker.sock
               name: docker-sock
          - name: git
            image: alpine/git:latest
            command:
            - cat
            tty: true
          volumes:
          - name: docker-sock
            hostPath:
              path: /var/run/docker.sock    
      '''
    }
  } 
    
    environment {
      REPO_NAME = 'saharyadgar/cicd'  
      DOCKERHUB_CREDENTIALS = credentials('dockerhub')
    }

    stages {
        stage('Increment version') {
            steps {
              container('docker') {
                script {
                  // Read the current version from the file
                  env.LAST_VERSION = readFile('version.txt').trim()
                  
                  // Parse the major, minor, and patch versions
                  def (major, minor, patch) = LAST_VERSION.tokenize('.')

                  // Increment the patch version
                  def newPatch = patch.toInteger() + 1

                  // Write the updated version to the file
                  writeFile(file: 'version.txt', text: "${major}.${minor}.${newPatch}\n")
                  env.NEW_VERSION = readFile('version.txt').trim()

                  // Print the new version number
                  echo "New version: ${major}.${minor}.${newPatch}"
                }
              } 
            }
        }

        stage('Build docker image') {
            steps {
              container('docker') {
                sh "docker build -t ${REPO_NAME}:${NEW_VERSION} ."
              } 
            }
        }
    
        stage('Push docker image') {
            steps {
              container('docker') {
                sh 'echo $DOCKERHUB_CREDENTIALS_PSW | docker login -u $DOCKERHUB_CREDENTIALS_USR --password-stdin'
                sh "docker push ${REPO_NAME}:${NEW_VERSION}"
              }
            }
        }

        stage ('Commit version update') {
          steps {
            container('git') {
               withCredentials([gitUsernamePassword(credentialsId: 'gitlab-usr-pwd', gitToolName: 'Default')]) {
                    sh "git config --global --add safe.directory /home/jenkins/agent/workspace/ci-cd"
                    sh 'git config --global user.email "jenkins@example.com"'
                    sh 'git config --global user.name "jenkins"'
                    sh "git checkout dev"
                    sh "git add version.txt"
                    sh 'git commit -m "Update version to ${NEW_VERSION}"'
                    sh "git push -u origin dev"
                }
            }
          }
        }
        
        stage('Change manifast file') {
            steps {
              container('git') {
                withCredentials([gitUsernamePassword(credentialsId: 'gitlab-usr-pwd', gitToolName: 'Default')]) {
                  checkout([$class: 'GitSCM', branches: [[name: '*/main']],
                  extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "manifasts"]], 
                  userRemoteConfigs: [[credentialsId: 'gitlab-usr-pwd', url: 'https://gitlab.com/saharyadgar12/configuration-files.git']]])
                  sh 'echo "image version: ${NEW_VERSION}\n last version: ${LAST_VERSION}"'
                  dir("manifasts") {
                    sh "git config --global --add safe.directory /home/jenkins/agent/workspace/ci-cd/manifasts"
                    sh "git checkout main"
                    sh 'sed -i "s/${LAST_VERSION}/${NEW_VERSION}/g" my-app/values.yaml'
                    sh "cat my-app/values.yaml"
                    sh "git add ."
                    sh 'git commit -m "Update version to ${NEW_VERSION}"'
                    sh "git push"
                  }
                }              
              }
            }
        }
    }

      post {
        always {
          container('docker') {
            sh 'docker logout'
          }
        }
        
        success {
          sh 'echo "Pipeline succeeded. Incremented version to ${NEW_VERSION}"' 
        }
        
        failure {
          sh 'echo "Pipeline failed. Not incremented version."'
        }
    }
}
